# LabCel - Experiments

Repository with source codes for programs used to analyze data from hydras experiments done in LabCel - IF/IFRGS.



## Requirements

- Octave (.m files)
- Python (.py files)




## Octave functions

### Dilate / Erode

- [dilate.m](./dilate.m)
  - function in octave to dilate (expand) white areas in binary images smoothing and reducing noise on borders
- [erode.m](./erode.m)
  - function in octave to erode (reduce) white areas in binary images smoothing and reducing noise on them on borders

### Img 2 BW

- [img2bw.m](./img2bw.m)
  - function to transform a given RGB (color) image in a BW (black and white, not grayscale) image allowing dilate/erode processing

### Foco

- [foco.m](./foco.m)
  - estimate focus quality in given images

### Junta

- [junta.m](./junta.m)
  - joins red filtered with green filtered images in one single image

### Normalize

- [normalize.m](./normalize.m)
  - allows image intensity normalization for better processing using logistic function or channel averages (according code comments/uncomments)

### Proc Img 2 BW, Proporcao Green / Red

- [procimg2bw.m](./procimg2bw.m)
  - process images in RGB (color), enhancing red channel intensity and applying img2bw and erode functions (above), applying [bwlabel](https://octave.sourceforge.io/image/function/bwlabel.html) function to processed images saving cluster identification results
- [proporcao.m](./proporcao.m)
  - same as `procimg2bw` but **without** red channel enhancement
- [proporcao_green.m](./proporcao_green.m)
  - same as `procimg2bw` but with **green** channel enhancement
- [proporcao_red.m](./proporcao_red.m)
  - similar to `procimg2bw` with diferent parameters

### Trata Todos (Modificado)

- [tratatodos_modificado.m](./tratatodos_modificado.m)
  - apply a intensity multiplier to all values in all channel in all given images

## Python scripts

### Massa Versus Diff

- [massaversusdiff.py](./massaversusdiff.py)
  - computes and plots diffusion by mass

### soh-graf

- [soh-graf.py](./soh-graf.py)
  - plot all dat files in folder and shows the resulting plot (for fast review)

### Soma Pixels

- [soma_pixels.py](./soma_pixels.py)
  - compute cluters sizes and center of mass (position) on given images
  - search new position and correlate position on previous with actual image, allowing cluster tracking 
