import os
import math as m
import sys
import matplotlib.pyplot as plt
p = os.popen('ls *dat')  
f=p.read()
f=f.replace("\n"," ")
f=f.split()
l=len(f)
xx=[]
size=0
for i in f:
    z=open(i)
    z=z.read()
    z=z.replace("\n"," ")
    z=z.split()
    z=map(float,z)
    x,y=[],[]
    for j,w in enumerate(z):
        if(j%3==0):
            x.append(w)
        if(j%3==1):
            y.append(w)
    plt.loglog(x,y)
    plt.title(i)
    plt.show()
