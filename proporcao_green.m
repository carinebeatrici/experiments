function proporcao_green (imgfile)
#c=red/green
i=imread(imgfile);
i=int8(i);
ni(:,:,2)=(-1.6.*i(:,:,1)+3.5*i(:,:,2)-2*i(:,:,3));
ni(:,:,1)=0;
ni(:,:,3)=0;
if(ni(:,:,1)>255) ni(:,:,1)=255; endif
if(ni(:,:,1)<0) ni(:,:,1)=0; endif
mask=(1/9)*ones(3);
i=uint8(ni);
bw=im2bw(i,graythresh(i));
mask=(1/9)*ones(3);
idil=dilate(bw,mask);
idil=dilate(idil,mask);
idil=dilate(idil,mask);
ie=erode(idil,mask);
ie=erode(ie,mask);
mat=bwlabel(ie,8);
imgdat=[imgfile,"-green.dat"];
save (imgdat,"mat");
imgfile=["bw-green-",imgfile];
imwrite (ie,imgfile);
