function proporcao_red (imgfile)
#c=red/green
i=imread(imgfile);
i=int8(i);
ni(:,:,1)=(2.5*i(:,:,1)-1.3*i(:,:,2)-2*i(:,:,3));
ni(:,:,2)=0;
ni(:,:,3)=0;
if(ni(:,:,1)>255) ni(:,:,1)=255; endif
if(ni(:,:,1)<0) ni(:,:,1)=0; endif
mask=(1/9)*ones(3);
i=uint8(ni);
bw=im2bw(i,graythresh(i));
idil=dilate(bw,mask);
idil=dilate(idil,mask);
idil=dilate(idil,mask);
ie=erode(idil,mask);
ie=erode(ie,mask);
mat=bwlabel(ie,8);
imgdat=[imgfile,"-red.dat"];
save (imgdat,"mat");
imgfile=["bw-red-",imgfile];
imwrite (ie,imgfile);
