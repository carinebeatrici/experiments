function img2bw(imgfile)

i=imread(imgfile);
imgnum=strrep(imgfile,".jpg","");
imgnum=strrep(imgfile,".png","")
bw=im2bw(i,graythresh(i,'moments'));
mask=(1/9)*ones(3);
er=erode(bw, mask);
id=dilate(er,mask);
mat=bwlabel(id,8);
imgdat=[imgnum,".dat"];
save (imgdat, "mat");
imgfile=["bw-",imgfile];
imwrite (id,imgfile);

endfunction

