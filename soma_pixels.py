#!/usr/bin/env python
#-*-coding: utf8 -*-
import sys
import math
import openpiv.tools
import openpiv.process
import openpiv.scaling
import openpiv.validation
import openpiv.filters
import numpy as np

imagepos={}
n=0

def soma_pixels(ni, nf, nalg,vmin, vmax):

        global imagepos, n

	for i in range (ni,nf+1,1):
		print str(i)+"/"+str(nf)
                
		fwrite="rcm-{0:0"+str(nalg)+"d}.dat"
		fwrite=fwrite.format(i)
		fwrite=open(fwrite,'w')

		fread="norm{0:0"+str(nalg)+"d}.jpg-green.dat"
		fread=fread.format(i)
		fread=open(fread,'r')
		lines=fread.read().splitlines()
		lwrite={}#declarando dicionario
		
		row=0
		mrcmx=0	#média dos centros de massa em x
		mrcmy=0 #média dos centros de massa em y
		posx={}
		posy={}
		pos={}
		rcm={} 
		rcmx={}		
		rcmy={}

		for line in lines:
			if len(line)>0 and line[0]!='#':
				row=row+1
				col=0
				line=line.strip()#para tirar os espacos no inicio e final da string
				split=line.split(" ")
				for v in split:
					col=col+1
					k=int(v)
					if k in lwrite:
						lwrite[k]=lwrite[k]+1
						posx[k]=posx[k]+row
						posy[k]=posy[k]+col
						pos[k]=math.sqrt(pow(posx[k],2)+pow(posy[k],2))
					else:
						lwrite[k]=1
						posx[k]=row
						posy[k]=col
						pos[k]=math.sqrt(pow(posx[k],2)+pow(posy[k],2))
		TOT=0
		del lwrite[0]
		del pos[0]
		del posx[0]
		del posy[0]		
		j=1	
                imagescm={}
		for w in sorted(lwrite):#dicionario ordenado por chaves(indices)
			if lwrite[w]>vmin:
				if lwrite[w]<vmax:
					
					rcm[w]=pos[w]/lwrite[w]
					rcmx[w]=posx[w]/lwrite[w]
					rcmy[w]=posy[w]/lwrite[w]
					mrcmx=mrcmx+rcmx[w]
					mrcmy=mrcmy+rcmy[w]
					lwrite[j]=lwrite[w]
					rcm[j]=rcm[w]
					rcmx[j]=rcmx[w]
					rcmy[j]=rcmy[w]
					fwrite.write("%d %f %f %f\n" % (j, lwrite[j], rcmx[j], rcmy[j]))
                                        imagescm[j]={'x': rcmx[j], 'y': rcmy[j]}
                                        #print rcmx[j], rcmy[j]
                                        n=j
                                        j=j+1

					#TOT=TOT+lwrite[w]
                if i<nf:
                    openPIV(i)
		#fwrite.write('TOT %f\n' % TOT 
                imagepos[i]=imagescm
                #print imagepos[i]

                print n, imagepos[i][j-1]["x"]

def openPIV(i):

   # for i in range (ni,nf+1,1):
   #print str(i)+"/"+str(nf)
    j=i+1
    fo="cv-{0:0"+str(nalg)+"d}.dat"
    fo=fo.format(i)
    fo=open(fo,'w')

    fra="bw-green-norm{0:0"+str(nalg)+"d}.jpg"
    fra=fra.format(i)
    fra=open(fra,'r')
    fra=openpiv.tools.imread(fra)

    frb="bw-green-norm{0:0"+str(nalg)+"d}.jpg"
    frb=frb.format(j)
    frb=open(frb,'r')
    frb=openpiv.tools.imread(frb)
    
    fra=np.int32(fra)
    frb=np.int32(frb)

    u, v, sig2noise = openpiv.process.extended_search_area_piv( fra, frb, window_size=12, overlap=6, dt=1, search_area_size=32, sig2noise_method='peak2peak' )

    x, y = openpiv.process.get_coordinates( image_size=fra.shape, window_size=12, overlap=6 )

    u, v, mask = openpiv.validation.sig2noise_val( u, v, sig2noise, threshold = 1.3 )

    u, v = openpiv.filters.replace_outliers( u, v, method='localmean', max_iter=10, kernel_size=2)

    x, y, u, v = openpiv.scaling.uniform(x, y, u, v, scaling_factor = 1.0)#96.52 )

    openpiv.tools.save(x, y, u, v, mask, fo)

#    openpiv.tools.display_vector_field(fo, scale=100, width=0.0025)


def cluster_index(ni, nf):

    global imagepos, n
    diffx={}
    diffy={}
    clust0x={}
    clust0y={}
    clustx={}
    clusty={}
    print "n", n
    for j in range (1, n+1):
        for i in range (ni, nf+1):
            clust0x=imagepos[i][j]["x"]
            clust0y=imagepos[i][j]["y"]
            if i<nf:
                diffx=imagepos[i][j]["x"]
                diffy=imagepos[i][j]["y"]
           # print clust0x, clust0y
            for k in range (1, n+1):
                if i<nf:
                    w=i+1
                    clustx=imagepos[w][k]["x"]
                    clusty=imagepos[w][k]["y"]
                    diffx[k]=clust0x-clustx
                    diffy[k]=clust0y-clusty
                    print diffx[i][k][w]
            #print diffx[k], diffy[k]
            #ind=min(diffx, key=diffx.get)
            #print ind

   # print min(diffx), min(diffy)
   # print max(diffx), max(diffy)
        

ni=int(sys.argv[1])#numero inicial
nf=int(sys.argv[2])#numero final
nalg=int(sys.argv[3])#numero de algarismos
vmin=int(sys.argv[4])#tamanho minimo de agregado
vmax=int(sys.argv[5])#tamanho maximo de agregado
soma_pixels(ni, nf, nalg, vmin, vmax)
#cluster_index(ni, nf)

