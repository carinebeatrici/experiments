function proporcao (imgfile,c)
#c=red/green
i=imread(imgfile);
bw=im2bw(i,graythresh(i));
mask=(1/9)*ones(3);
idil=dilate(bw,mask);
idil=dilate(idil,mask);
idil=dilate(idil,mask);
ie=erode(idil,mask);
ie=erode(ie,mask);
mat=bwlabel(ie,8);
imgdat=[imgfile,"-",c,".dat"];
save (imgdat,"mat");
imgfile=["bw-",c,"-",imgfile];
imwrite (ie,imgfile);
