import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

massa,slope,intercept,r1,r2,r3=[],[],[],[],[],[]
lista_s1, lista_s2, lista_s3, lista_s4 = [], [], [], []
lista_m1, lista_m2, lista_m3, lista_m4 = [], [], [], []
lista_I1, lista_I2, lista_I3, lista_I4 = [], [], [], []
#lista_s1, lista_s2, lista3, lista4 = [], [], [], []
with open('saida_dif.dic') as fi:
	for z_list in fi:
		massa.append(float(z_list.split("-")[2])),slope.append(float(z_list.split()[1])),intercept.append(float(z_list.split()[2])),r1.append(float(z_list.split()[3])),r2.append(float(z_list.split()[4])),r3.append(float(z_list.split()[5]))
	for i,w in enumerate(massa):
		if w < 100:
                        lista_I1.append(np.exp(intercept[i]))
                        lista_s1.append(slope[i])
                        lista_m1.append(w)
		if w > 100 and w < 350:
                        lista_I2.append(np.exp(intercept[i]))                        
                        lista_s2.append(slope[i])
                        lista_m2.append(w)
		if w > 350 and w < 600:
                        lista_s3.append(slope[i])
                        lista_I3.append(np.exp(intercept[i]))
                        lista_m3.append(w)
		if w > 600:
                        lista_I4.append(np.exp(intercept[i]))
                        lista_s4.append(slope[i])
                        lista_m4.append(w)

#plt.scatter(massa,slope)
#plt.show()

media_s=[sum(lista_s1)/len(lista_s1),sum(lista_s2)/len(lista_s2),sum(lista_s3)/len(lista_s3),sum(lista_s4)/len(lista_s4)]
dev_s =[np.std(lista_s1)/2,np.std(lista_s2)/2,np.std(lista_s3)/2,np.std(lista_s4)/2]
massa_list = [np.mean(lista_m1),np.mean(lista_m2),np.mean(lista_m3),np.mean(lista_m4)]
media_I=[sum(lista_I1)/len(lista_I1),sum(lista_I2)/len(lista_I2),sum(lista_I3)/len(lista_I3),sum(lista_I4)/len(lista_I4)]
dev_I =[np.std(lista_I1)/2,np.std(lista_I2)/2,np.std(lista_I3)/2,np.std(lista_I4)/2]

slope_I,intercept_I,r_value_I,p_value_I,std_err_I = stats.linregress(massa_list,media_I)

slope,intercept,r_value,p_value,std_err = stats.linregress(massa_list,media_s)

x=np.arange(17,1000,10)
z=[]
for i in x :
    z.append(intercept_I+slope_I*i)   
zz=[]
for i in x :
    zz.append(intercept+slope*i)   
zinv=[]
for i in x:
	zinv.append(17*media_I[0]/i)
fig, ax = plt.subplots(3)
fig.tight_layout()
fig.suptitle('Expoente e constante de difusão')

 
#ax[0].rc('axes', labelsize=16)
#plt.axis([0,1000,-1,3])
ax[0].errorbar(massa_list, media_I, yerr=dev_I,fmt='o')
lab = "inclinação=%.4f"%(slope_I)
ax[0].plot(x,z,label=lab)
ax[0].set(ylabel=r"D")


ax[0].legend()
#ax[0].plot(x,z)
#ax[0].plot(x,zinv)
#ax[1].loglog(x,z)
lab="1/m"
ax[1].loglog(x,zinv,label=lab)
lab="D medido"
ax[1].loglog(massa_list, media_I, "--b",label=lab)# yerr=dev_I,fmt='o')
#ax[0].set(xlabel="mass")
ax[1].set(ylabel=r"log(D)")
ax[1].legend()
#ax[1].rc('axes', labelsize=16)
#plt.axis([0,1000,-1,3])
lab=(("media=%.2f, desvio=%.2f" )%(np.mean(media_s),np.std(media_s)))
ax[2].errorbar(massa_list, media_s, yerr=dev_s, fmt='o')
#lab = "inclinação_expoente=%.4f, r=%f, p=%f"%(slope,r_value,p_value)
ax[2].plot(x,zz,label=lab)

ax[2].set(xlabel="mass")
ax[2].set(ylabel=r"expoente")
ax[2].legend()
plt.show()
