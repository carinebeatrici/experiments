function fim=junta(numero_de_imagens)
for i=1:2:numero_de_imagens;
  nomeverde = int2str(i);
  nomevermelha = int2str(i+1);
  nomesaida = int2str(1+floor(i/2));
  if (i<9999);    
    if(i<1000);
      nomeverde = strcat("0",nomeverde);
      nomevermelha = strcat("0",nomevermelha);
    endif;
    if(i<99);
      nomeverde = strcat("0",nomeverde);
      nomevermelha = strcat("0",nomevermelha);
    endif;
    if(i==99); 
      nomeverde = strcat("0",nomeverde);
    endif;
    if (i==9);
      nomeverde = strcat("0",nomeverde);
    endif;
    if(i<9);
      nomeverde = strcat("0",nomeverde);
      nomevermelha = strcat("0",nomevermelha);
    endif;
      nomeverde = strcat("flog-norm-norm-n",nomeverde,".jpg")
      nomevermelha = strcat("flog-norm-norm-n",nomevermelha,".jpg")
    ImagemG=imread(nomeverde);
    ImagemR=imread(nomevermelha);
    Imagem=uint8(ImagemG+ImagemR);
  endif;
  if(1+floor(i/2)<1000);
    nomesaida = strcat("0",nomesaida);
  endif;
  if(1+floor(i/2)<100);
    nomesaida = strcat("0",nomesaida);
  endif;
  if(1+floor(i/2)<10);
    nomesaida = strcat("0",nomesaida);
  endif;
  nomesaida = strcat("s-flog-norm-",nomesaida,".jpg");
  imwrite(Imagem,nomesaida);
end;
fim=1;


endfunction;
  
