function procimg2bw (imgfile)
i=imread(imgfile);
i=int8(i);
ni(:,:,1)=(3.5*i(:,:,1)-1.5*i(:,:,2)-2*i(:,:,3));
ni(:,:,2)=0;
ni(:,:,3)=0;
if(ni(:,:,1)>255) ni(:,:,1)=255; endif
if(ni(:,:,1)<0) ni(:,:,1)=0; endif
mask=(1/9)*ones(3);
i=uint8(ni);
bw=im2bw(i,graythresh(i));
id=dilate(bw,mask);
#imshow(bw);
mat=bwlabel(id,8);
imgdat=[imgfile,".dat"];
save (imgdat, "mat");
imgfile=["bw-",imgfile];
imwrite (id,imgfile);

endfunction

