function BW2 = erode(BW1, SE, a, b)
  alg='spatial';
  n=1;
  if (nargin < 1 || nargin > 4)
    usage ("BW2 = erode(BW1, SE [, alg] [, n])");
  endif
  if nargin ==  4
    alg=a;
    n=b;
  elseif nargin == 3
    if ischar(a)
      alg=a;
    else
      n=a;
    endif
  endif
  
  if !strcmp(alg, 'spatial')
    error("erode: alg not implemented.");
  endif
  
  # count ones in mask
  thr=sum(SE(:));
  
  # "Binarize" BW1, just in case image is not [1,0]
  BW1=BW1!=0;
  
  for i=1:n
    # create result matrix
    BW1=filter2(SE,BW1) == thr;
  endfor
  
  BW2=BW1;
endfunction
